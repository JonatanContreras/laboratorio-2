var turno = "X";
var totalJugadas = 0;

function iniciarJuego() {
  let i;

  let formTablero = document.getElementById("formTablero");
  
  for (i = 1; i < 10; i++) {
    let miBoton = document.createElement("input");
    miBoton.type = "button";
    miBoton.value = " ";
    miBoton.setAttribute("id", "boton" + i);
    miBoton.setAttribute("class", "boton");
    miBoton.setAttribute("onClick", "realizarJugada(this.id)");
    formTablero.appendChild(miBoton);
    if (i % 3 === 0) {
      let salto = document.createElement("br");
      formTablero.appendChild(salto);
    }
  }
}

function realizarJugada(idBoton) {
  let resultado, triqui;
  resultado = marcarBoton(idBoton);

  if (resultado == true) {
    totalJugadas++;
    triqui = calcularTriqui();
    if (triqui) {
      alert("Has ganado " + turno + " !!!!");
      borrarTablero();
    } else if (totalJugadas == 9) {
      alert("Ocurrió un empate!!!");
      borrarTablero();
    } else {
      cambiarTurno();
    }
  }

  return false;
}

function cambiarTurno() {
  if (turno === "X") {
    turno = "O";
  } else {
    turno = "X";
  }
}

function marcarBoton(idBoton) {
  let boton = document.getElementById(idBoton);

  if (boton.value === "X" || boton.value === "O") {
    alert("Boton ocupado. Por favor seleccione otra.");
    return false;
  }
  boton.value = turno;

  return true;
}

function calcularTriqui() {
  //Calcular triqui en filas
  let i;
  let boton1;
  let boton2;
  let boton3;

  for (i = 0; i < 9; i = i + 3) {
    boton1 = document.getElementById("boton" + (i + 1)).value;
    boton2 = document.getElementById("boton" + (i + 2)).value;
    boton3 = document.getElementById("boton" + (i + 3)).value;
    if (boton1 === boton2 && boton1 === boton3 && boton1 != " ") {
      return true;
    }
  }

  //Calcular triqui en columnas
  for (i = 1; i < 4; i++) {
    boton1 = document.getElementById("boton" + i).value;
    boton2 = document.getElementById("boton" + (i + 3)).value;
    boton3 = document.getElementById("boton" + (i + 6)).value;
    if (boton1 === boton2 && boton1 === boton3 && boton1 != " ") {
      return true;
    }
  }

  //Calcular triqui en diagonales

  let valor = [2, 4];
  for (i = 0; i < 2; i++) {
    boton1 = document.getElementById("boton" + 5).value;
    boton2 = document.getElementById("boton" + (5 + valor[i])).value;
    boton3 = document.getElementById("boton" + (5 - valor[i])).value;
    if (boton1 === boton2 && boton1 === boton3 && boton1 != " ") {
      return true;
    }
  }

  return false;
}

function borrarTablero() {
  totalJugadas = 0;
  turno = "X";
  for (i = 1; i < 10; i++) {
    boton = document.getElementById("boton" + i);
    boton.value = " ";
  }
}
